const redis = require("redis");

const timeoutPromise = time =>
  new Promise((resolve, reject) =>
    setTimeout(() => reject("Promise timed out"), time)
  );

const timeout = (promise, time) =>
  Promise.race([promise, timeoutPromise(time)]);

const start = async () => {
  const client = redis.createClient({
    port: 6379
  });
  client.set("foo", "bar");

  while (true) {
    try {
      //   console.log(await timeout(client.get("foo"), 300));
      console.log(
        await new Promise((resolve, reject) =>
          client.get("foo", (err, result) =>
            err ? reject(err) : resolve(result)
          )
        )
      );
    } catch (err) {
      console.log(err);
    }
    await new Promise(resolve => setTimeout(resolve, 1000));
  }
};

start();
