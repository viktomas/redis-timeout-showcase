const Redis = require("ioredis");

const timeoutPromise = time =>
  new Promise((resolve, reject) =>
    setTimeout(() => reject("Promise timed out"), time)
  );

const timeout = (promise, time) =>
  Promise.race([promise, timeoutPromise(time)]);

const start = async () => {
  const client = new Redis({
    port: 6379,
    connectTimeout: 3000, //doesn't help with established connection
    maxRetriesPerRequest: 3, // when server is asleep, client doesn't try to reconnect
    enableOfflineQueue: false // this only works when the connection isn't established
  });
  client.set("foo", "bar");

  while (true) {
    try {
      console.log(await timeout(client.get("foo"), 300));
    } catch (err) {
      console.log(err);
    }
    await new Promise(resolve => setTimeout(resolve, 1000));
  }
};

start();
