# Simple test of redis client timeouts

We've noticed that Gitter redis client hangs indefinitely when redis server stops responding.

This repo introduces two tiny examples for `ioredis` and `redis` npm libraries to test timing out.

## How to run the examples

1. Have `redis-server` and `redis-cli` available locally.
2. Start redis server `redis-server`
3. Run one of the examples
    - `npm run redis`
    - `npm run ioredis`
4. Now you should see the example outputing `bar` (value of the key `foo`) every second
5. You can simulate non responding server by running `redis-cli debug sleep 30`
6. See the client hanging for 30 seconds
